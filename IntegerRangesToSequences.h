#pragma once
#include <string>

namespace IntegerRangesToSequences {
  std::string buildRanges(std::vector<int> & array) {
    std::stringstream ss;
    int startSeq = array[0];
    for (unsigned int i = 1; i < array.size(); ++i) {
      if (array[i - 1] != array[i] - 1) {
        //flush sequence, between 
        if (startSeq != array[i - 1]) {
          ss << startSeq << '-' << array[i - 1] << ',';
        } else {
          ss << startSeq << ',';
        }
        startSeq = array[i];
      }
    }

    if (startSeq != array.back()) {
      ss << startSeq << '-' << array.back();
    } else {
      ss << startSeq;
    }
    return ss.str();
  }
}