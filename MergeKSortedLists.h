#pragma once
#include <algorithm>
#include <vector>
namespace MergeKSortedLists {
  //Definition for singly-linked list.
  struct ListNode {
      int val;
      ListNode *next;
      ListNode(int x) : val(x), next(NULL) {}
  };

  class Solution {
  public:
    static bool mycomparator(ListNode * left, ListNode* right) { return left->val > right->val; }
    static ListNode* mergeKLists(std::vector<ListNode*>& lists) {
      if (lists.empty()) return NULL;
      std::make_heap(lists.begin(), lists.end(), &mycomparator);

      std::pop_heap(lists.begin(), lists.end(), &mycomparator);
      ListNode* head = lists.back();
      if (head == NULL) return NULL;
      if (head->next == NULL) {
        lists.pop_back();
      } else {
        lists.back() = head->next;
        std::push_heap(lists.begin(), lists.end(), &mycomparator);
      }

      ListNode* current = head;
      while (!lists.empty()) {
        std::pop_heap(lists.begin(), lists.end(), &mycomparator);
        ListNode* smallest = lists.back();
        if (smallest->next == NULL) {
          lists.pop_back();
        } else {
          lists.back() = smallest->next;
          std::push_heap(lists.begin(), lists.end(), &mycomparator);
        }
        current->next = smallest;
        current = smallest;
      }
      return head;
    }
  };
}