#pragma once

#include "KMins.h"
#include "gtest\gtest.h"
#include <algorithm>

TEST(pb17_14, test1) {
  std::vector<int> v{ 9,8,7,6,5,4,3,2,1,0 };
  KMins::kminsInPlace(v, 3);
  for (int i = 0; i < 3; ++i) {
    EXPECT_LE(std::find(v.begin(),v.end(),i), v.begin() + 3);
  }
}

TEST(pb17_14, test2) {
  std::vector<int> v{ 7,3,1,2,13,14,4,5,12,6,8,9,10,11,0 };
  KMins::kminsInPlace(v, 6);
  for (int i = 0; i < 6; ++i) {
    EXPECT_LE(std::find(v.begin(), v.end(), i), v.begin() + 6);
  }
}