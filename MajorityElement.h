#pragma once
#include <vector>

using namespace std; 
namespace MajorityElement {
  int majorityElement(const vector<int>& arr) {
    vector<long> bitsMap(8 * sizeof(int));

    for (int v : arr) {
      for (int shift = 0; shift < 8 * sizeof(int); ++shift) {
        bitsMap[shift] += (v >> shift) & 1;
      }
    }

    int threshold = (int)floor(arr.size() / 2) + 1;
    int potentialMajority = 0;
    for (int shift = 0; shift < 8 * sizeof(int); ++shift) {
      if (bitsMap[shift] >= threshold) {
        potentialMajority += 1 << shift;
      }
    }

    int realCount = 0;
    for (int v : arr) {
      if (v == potentialMajority) realCount++;
    }

    if (realCount >= threshold) return potentialMajority;
    else return -1;
  }
}