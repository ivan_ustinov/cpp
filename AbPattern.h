#pragma once
#include <string>

class AbPattern
{
public:
  AbPattern();
  static bool check(const std::string &pattern, const std::string &str);
  static bool check(const std::string &pattern, const std::string &str, int a_n, int b_n, int a_word_size, int b_word_size);
  ~AbPattern();
};

