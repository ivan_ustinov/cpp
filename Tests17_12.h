#pragma once

#include "BiNode.h"
#include "gtest\gtest.h"

/*        -------4--------
*     ---2---           5----
*  ---1     3               6
*  0
*/

TEST(pb17_12, test1) {
  BiNode v0(0);
  BiNode v1(1,&v0,nullptr);
  BiNode v3(3);
  BiNode v2(2, &v1, &v3);

  BiNode v6(6);
  BiNode v5(5, nullptr, &v6);
  BiNode v4(4, &v2, &v5);

  BiNode * it = BiNode::convertTreeToList(&v4);
  EXPECT_EQ(it->getLeft(), nullptr);
  int i = 0;
  while (it != nullptr) {
     EXPECT_EQ(it->getData(), i++);
     it = it->getRight();
  }
}