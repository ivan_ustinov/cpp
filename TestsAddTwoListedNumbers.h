#pragma once

#include "AddTwoListedNumbers.h"
#include "gtest\gtest.h"

TEST(pbTwoListed, test1) {
  //12
  AddTwoListedNumbers::ListNode * n1 = new AddTwoListedNumbers::ListNode(1);
  AddTwoListedNumbers::ListNode * n2 = new AddTwoListedNumbers::ListNode(2);
  n2->next = n1;
  //34
  AddTwoListedNumbers::ListNode * n3 = new AddTwoListedNumbers::ListNode(3);
  AddTwoListedNumbers::ListNode * n4 = new AddTwoListedNumbers::ListNode(4);
  n4->next = n3;

  AddTwoListedNumbers::ListNode * res = AddTwoListedNumbers::Solution::addTwoNumbers(n2, n4);
  
  EXPECT_EQ(res->val, 6);
  EXPECT_EQ(res->next->val, 4);
}


TEST(pbTwoListed, test2) {
  //342
  AddTwoListedNumbers::ListNode * n1 = new AddTwoListedNumbers::ListNode(2);
  AddTwoListedNumbers::ListNode * n2 = new AddTwoListedNumbers::ListNode(4);
  AddTwoListedNumbers::ListNode * n3 = new AddTwoListedNumbers::ListNode(3);
  n2->next = n1;
  n3->next = n2;
  //465
  AddTwoListedNumbers::ListNode * n4 = new AddTwoListedNumbers::ListNode(5);
  AddTwoListedNumbers::ListNode * n5 = new AddTwoListedNumbers::ListNode(6);
  AddTwoListedNumbers::ListNode * n6 = new AddTwoListedNumbers::ListNode(4);
  n6->next = n5;
  n5->next = n4;

  AddTwoListedNumbers::ListNode * res = AddTwoListedNumbers::Solution::addTwoNumbers(n3, n6);

  EXPECT_EQ(res->val, 7);
  EXPECT_EQ(res->next->val, 0);
  EXPECT_EQ(res->next->next->val, 8);
}