#pragma once

#define TEST_FRIENDS \
    friend class noninitialisation_ct_vector_Test;

#include "ConstTimeAllocationVector.h"
#include "CountTwos.h"
#include "gtest\gtest.h"

class CustomInt {
public:
  int v;
  CustomInt() {
    v = 42;
   // std::cout << "CustomInt()" << std::endl;
  };
  CustomInt(int v) {
    this->v = v;
  };
};

TEST(ct_vector_test_case, ct_vector_test)
{
  ConstTimeAllocationVector<CustomInt> v(512);

  EXPECT_EQ(v.get(7).v, 42);

  v.get(7).v = 7;
  EXPECT_EQ(v.get(7).v, 7);
  v.get(6).v = 6;
  EXPECT_EQ(v.get(6).v, 6);
  v.get(5).v = 5;
  EXPECT_EQ(v.get(5).v, 5);
  EXPECT_EQ(v.get(4).v, 42);
}

TEST(noninitialisation, ct_vector)
{
  ConstTimeAllocationVector<CustomInt> v(512);
  EXPECT_NE(v.vector[0].t.v, 42);
  EXPECT_NE(v.vector[1].t.v, 42);
  EXPECT_NE(v.vector[2].t.v, 42);
  EXPECT_NE(v.vector[3].t.v, 42);
}

TEST(pb17_6, test1) {
  EXPECT_EQ(CountTwos::Count2s(0), 0);

  EXPECT_EQ(CountTwos::Count2s(1), 0);
  EXPECT_EQ(CountTwos::Count2s(2), 1);
  EXPECT_EQ(CountTwos::Count2s(11), 1);
  EXPECT_EQ(CountTwos::Count2s(12), 2);
}

TEST(pb17_6, test2) {
  EXPECT_EQ(CountTwos::Count2s(102), 21);
}

TEST(pb17_6, test3) {
  EXPECT_EQ(CountTwos::Count2sBrutForce(102), 21);
}

TEST(pb17_6, test4) {
  EXPECT_EQ(CountTwos::Count2s(202), 44);
}

TEST(pb17_6, test5) {
  EXPECT_EQ(CountTwos::Count2sBrutForce(202), 44);
}

TEST(pb17_6, test6) {
  EXPECT_EQ(CountTwos::Count2s(2102), CountTwos::Count2sBrutForce(2102));
}

TEST(pb17_6, test7) {
  EXPECT_EQ(CountTwos::Count2s(398502), CountTwos::Count2sBrutForce(398502));
}