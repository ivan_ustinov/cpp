#pragma once

#include <vector>
#include <stack>
#include <queue>

using namespace std;
class GraphTraversal {
private:
  vector<int> visited;
public:
  int findCircleNum(vector<vector<int>>& M) {
    visited.resize(M.size());
    int connectedComponentNumber = 0;
    for (int s = 0; s < M.size(); ++s) {
      if (visited[s] == 0) {
        connectedComponentNumber++;
        BFS(M, s);
      }
    }
    return connectedComponentNumber;
  }

  void BFS(vector<vector<int>>& M, int s) {
    queue<int> processing;
    processing.push(s);
    visited[s] = s + 1;
    while (!processing.empty()) {
      int v = processing.front();
      for (int i = 0; i < M.size(); ++i) {
        if (M[v][i] == 1 && visited[i] == 0) {
          processing.push(i);
          visited[i] = s + 1;
        }
      }
      processing.pop();
    }
  }

  void DFS(vector<vector<int>>& M, int s) {
    stack<int> processing;
    processing.push(s);
    visited[s] = s + 1;
    while (!processing.empty()) {
      int v = processing.front();
      for (int i = 0; i < M.size(); ++i) {
        if (M[v][i] == 1 && visited[i] == 0) {
          processing.push(i);
          visited[i] = s + 1;
        }
      }
      processing.pop();
    }
  }
};