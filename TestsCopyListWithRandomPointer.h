#pragma once
#include "CopyListWithRandomPointer.h"
#include "gtest\gtest.h"

TEST(pbLeetcode138, test1) {
  CopyListWithRandomPointer::RandomListNode l1(1);
  CopyListWithRandomPointer::RandomListNode l2(2);
  CopyListWithRandomPointer::RandomListNode l3(3);
  CopyListWithRandomPointer::RandomListNode l4(4);

  l1.next = &l2;
  l2.next = &l3;
  l3.next = &l4;

  l1.random = &l4;
  l4.random = &l1;

  CopyListWithRandomPointer::RandomListNode * ln1 = CopyListWithRandomPointer::Solution::copyRandomList(&l1);
  for (auto i = ln1, j = &l1; i != nullptr && j != nullptr; i = i->next, j = j->next) {
    EXPECT_EQ(i->label, j->label);
  }
}
