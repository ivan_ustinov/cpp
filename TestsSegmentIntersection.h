#pragma once

#include "SegmentIntersection.h"
#include "gtest\gtest.h"

TEST(pbSegmentIntersection, test1) {
  std::vector<std::pair<int, int>> arr = { {1,4}, {2,3}, {7,9}, {6, 8} };
  std::vector<std::pair<int, int>> expectedRes = { {1, 4},{6, 9} };
  std::vector<std::pair<int, int>> res = SegmentIntersection::joinSegments(arr);
  EXPECT_EQ(res.size(), expectedRes.size());
  for (unsigned int i = 0; i < res.size(); ++i) {
    EXPECT_EQ(res[i].first, expectedRes[i].first);
    EXPECT_EQ(res[i].second, expectedRes[i].second);
  }
}