#pragma once
#include <string>
#include <unordered_map>

namespace LenghtOfLongestSubstring {
  int  lenghtOfLongestSubstring(const std::string & str) {
    if (str.size() < 1) return 0;
    if (str.size() < 2) return 1;

    std::vector<int> charsCount(256);
    charsCount[str[0]]++;
    charsCount[str[1]]++;

    auto itBeginSubstr = str.begin();
    auto itEndSubstr = str.begin() + 2;

    //get in a state when substring contains two different charachters.
    bool startSeq = (str[0] == str[1]);
    while (itEndSubstr != str.end() && *(std::prev(itEndSubstr)) == str[0]) {
      charsCount[*(itEndSubstr)]++;
      itEndSubstr++;
    }

    int maxLenght = itEndSubstr - itBeginSubstr;
    //iterate over a string keeping only two different characters in the substring [itBeginSubstr,itEndSubstr)
    while (itEndSubstr != str.end()) {
      if (charsCount[*itEndSubstr] == 0) {
        while (charsCount[*itBeginSubstr] != 1) {
          charsCount[*itBeginSubstr]--;
          itBeginSubstr++;
        }
        charsCount[*itBeginSubstr]--;
        itBeginSubstr++;
      }
      charsCount[*itEndSubstr]++;
      itEndSubstr++;
      if (itEndSubstr - itBeginSubstr > maxLenght) maxLenght = itEndSubstr - itBeginSubstr;
    }
    return maxLenght;
  }
}
