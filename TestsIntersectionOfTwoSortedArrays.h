#pragma once
#include "IntersectionOfTwoSortedArrays.h"
#include "gtest\gtest.h"

TEST(pbIntersectionOfTwoSortedArrays, test1) {
  std::vector<int> v1{1,2,3,4,5,6,7};
  std::vector<int> v2{ 2,3,4,6 };
  std::vector<int> v3 = IntersectionOfTwoSortedArrays::stdIntersect(v1, v2);
  EXPECT_EQ(v2.size(), v3.size());
  for (unsigned int i = 0; i < v2.size(); ++i) {
    EXPECT_EQ(v2[i], v3[i]);
  }
}

