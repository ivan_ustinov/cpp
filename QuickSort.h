#pragma once
#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class QuickSort
{
public:
  QuickSort();
  ~QuickSort();
private:
  static void printVec(const vector<int> &vec) {
    for (int v : vec) {
      std::cout << v << " ";
    }
    cout << endl;
  }

  static void printVec(vector<int>::iterator beginPos, vector<int>::iterator endPos) {
    for (auto it = beginPos; it != endPos; ++it) {
      std::cout << *it << " ";
    }
    cout << endl;
  }

public:
  static void quickSort(vector<int>::iterator beginPos, vector<int>::iterator endPos) {
    if (beginPos == endPos) return;
    cout << "going to partion : ";
    printVec(beginPos, next(endPos));
    //get pivot element
    auto pivot = endPos;
    //place all the elements bigger than pivot at right of the pivot
    for (auto it = prev(endPos);; --it) {
      if (*pivot < *it) {
        cout << "going to swap pivot=" << *pivot << " it=" << *it << " and elem=" << *(prev(pivot)) << endl;
        cout << "before swap ";
        printVec(beginPos, next(endPos));
        int tmp = *pivot;
        *pivot = *it;

        *it = *(prev(pivot));
        *prev(pivot) = tmp;
        --pivot;
        cout << "after swap  ";
        printVec(beginPos, next(endPos));
      }
      if (it == beginPos) break;
    }
    cout << "partitioning done : ";
    printVec(beginPos, next(endPos));
    //sort left side
    if (pivot != beginPos) quickSort(beginPos, prev(pivot));
    //sort right side
    if(pivot != endPos) quickSort(next(pivot), endPos);
  }

  static int test() {
    cout << "unsorted array: ";
    vector<int> vec = { 8,3,6,1,5,0,5,2 };

    printVec(vec);
    quickSort(vec.begin(), prev(vec.end()));

    cout << "quick sorted array: ";

    printVec(vec);
    return 0;
  }
};

