#pragma once
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

#include "gtest\gtest.h"

class Graph {
public:
  class Node {
  public:
    Node(std::string s) {
      libName = s;
    }
    std::string libName;
    std::vector<Node *> neighbors;
  };
  enum State {
    visiting,visited,built,readyToBuild
  };
  static std::vector<std::string> dfs(Node * root) {
    std::vector<std::string> buildOrder; //resulting list of libraries in order of dependencies
    std::stack<Node *> processingNodes; //stack of Nodes that will be build, size of the stack <= the longest dependency chain
    std::unordered_map<Node *, State> nodesState; 

    processingNodes.push(root);
    nodesState[root] = visiting;

    while (!processingNodes.empty()) {
      Node * current = processingNodes.top();
      bool isReadyToBuild = true; //Doesn't have any non built dependent library 
      for (Node * n : current->neighbors) {
        std::unordered_map<Node *, State>::iterator childIt = nodesState.find(n);
        if (childIt == nodesState.end()) {
          //add dependent libs to tasks
          processingNodes.push(n);
          nodesState[n] = visiting;
          isReadyToBuild = false;
          break;
        } else if (childIt->second == visiting) {
          return getCycle(current, childIt, processingNodes);
        }
      }
      if (isReadyToBuild) {
        //build the current lib because it doesn't have dependent libraries
        buildOrder.push_back(current->libName);
        processingNodes.pop();
        nodesState[current] = visited;
      }
    }
    return buildOrder;
  };
private:
  static inline std::vector<std::string> getCycle(Node * current, std::unordered_map<Node *, State>::iterator & childIt, std::stack<Node *> & processingNodes) {
    std::vector<std::string> buildOrder; 
    std::ostringstream cycleErrorMessage;
    cycleErrorMessage << "Cycle dependency detected : " << childIt->first->libName << "<-";
    for (; current != childIt->first; ) {
      cycleErrorMessage << current->libName << "<-";
      processingNodes.pop();
      current = processingNodes.top();
    }
    cycleErrorMessage << current->libName;
    buildOrder.push_back(cycleErrorMessage.str());
    return buildOrder;
  }
};

TEST(simpleGraph, Graph)
{
  /*
  * n1
  *   ---n2
  *       ---n4
  *       ---n5
  *   ---n3
  *     
  */
  Graph::Node n5("n5");
  Graph::Node n4("n4");
  Graph::Node n3("n3");
  Graph::Node n2("n2");
  n2.neighbors.push_back(&n4);
  n2.neighbors.push_back(&n5);
  Graph::Node n1("n1");
  n1.neighbors.push_back(&n2);
  n1.neighbors.push_back(&n3);

  std::vector<std::string> result = Graph::dfs(&n1);

  EXPECT_EQ(result.size(), 5);
  EXPECT_EQ(result[0], n4.libName);
  EXPECT_EQ(result[1], n5.libName);
  EXPECT_EQ(result[2], n2.libName);
  EXPECT_EQ(result[3], n3.libName);
  EXPECT_EQ(result[4], n1.libName);
}

TEST(cycleDetection, Graph)
{
  /*
  * n1
  *   ---n2
  *       ---n4
  *       ---n5
  *   ---n3
  *       ---n1
  *
  */
  Graph::Node n5("n5");
  Graph::Node n4("n4");
  Graph::Node n3("n3");
  Graph::Node n2("n2");
  n2.neighbors.push_back(&n4);
  n2.neighbors.push_back(&n5);
  Graph::Node n1("n1");
  n1.neighbors.push_back(&n2);
  n1.neighbors.push_back(&n3);

  n3.neighbors.push_back(&n1);

  std::vector<std::string> result = Graph::dfs(&n1);

  EXPECT_EQ(result.size(), 1);
  EXPECT_EQ(result[0], "Cycle dependency detected : n1<-n3<-n1");
}

TEST(shortCycleDetection, Graph)
{
  /*
  * n1
  *   ---n2
  *       ---n4
  *       ---n5
  *   ---n3
  *   ---n1
  *
  */
  Graph::Node n5("n5");
  Graph::Node n4("n4");
  Graph::Node n3("n3");
  Graph::Node n2("n2");
  n2.neighbors.push_back(&n4);
  n2.neighbors.push_back(&n5);
  Graph::Node n1("n1");
  n1.neighbors.push_back(&n2);
  n1.neighbors.push_back(&n3);

  n1.neighbors.push_back(&n1);

  std::vector<std::string> result = Graph::dfs(&n1);

  EXPECT_EQ(result.size(), 1);
  EXPECT_EQ(result[0], "Cycle dependency detected : n1<-n1");
}