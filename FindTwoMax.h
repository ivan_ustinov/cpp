#pragma once
#include <vector>

namespace FindTwoMax{
  template <typename T>
  void FindTopTwo(const std::vector<T>& vec, T* max, T* next_max) {
    std::vector<T> maxPair(2);
    if (vec.size() < 2) {return;}
    int k = 0;
    maxPair[0] = vec[0];
    for (int i = 1; (unsigned int)i < vec.size(); ++i) {
      if (maxPair[k] <= vec[i]) {
        k ^= 1;
        maxPair[k] = vec[i];
      }
    }
    *max = maxPair[k];
    *next_max = maxPair[k ^ 1];
  };
}