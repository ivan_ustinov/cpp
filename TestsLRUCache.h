#pragma once
#include "LRUCache.h"
#include "gtest\gtest.h"

TEST(pbLRUCache, test1) {

  LRUCache cache(2 /* capacity */);

  cache.put(1, 1);
  cache.put(2, 2);
  EXPECT_EQ(cache.get(1),1);       // returns 1
  cache.put(3, 3);                 // evicts key 2
  EXPECT_EQ(cache.get(2),-1);      // returns -1 (not found)
  cache.put(4, 4);                 // evicts key 1
  EXPECT_EQ(cache.get(1),-1);      // returns -1 (not found)
  EXPECT_EQ(cache.get(3), 3);      // returns 3
  EXPECT_EQ(cache.get(4), 4);      // returns 4
}

TEST(pbLRUCache, test2) {
  //["LRUCache","put","get","put","get","get"]
  //[[1], [2, 1], [2], [3, 2], [2], [3]]
  LRUCache cache(1 /* capacity */);

  cache.put(2, 1);
  EXPECT_EQ(cache.get(2), 1);       // returns 1
  cache.put(3, 2);                 // evicts key 2
  EXPECT_EQ(cache.get(2), -1);      // returns -1
  EXPECT_EQ(cache.get(3), 2);      // returns 2
}