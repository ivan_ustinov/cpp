#pragma once
#include "TopKFrequent.h"
#include "gtest\gtest.h"
#include <set>
TEST(pbTopKFrequent, test1) {
  std::vector<int> v = { 6, 0, 1, 4, 9, 7, -3, 1, -4, -8, 4, -7, -3, 3, 2, -3, 9, 5, -4, 0 };
  std::vector<int> r = TopKFrequent::topKFrequent(v, 6);
  std::set<int> expected = { -3, -4, 4, 0, 1, 9 };

  for (unsigned int i = 0; i < r.size(); ++i) {
    EXPECT_NE(expected.find(r[i]), expected.end());
  }
}