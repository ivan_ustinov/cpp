#pragma once
#include <memory>
class BiNode {
private:
  
  BiNode * _left;
  BiNode * _right;
  int _data;
  
public:

  BiNode(int data) {
    _left = nullptr;
    _right = nullptr;
    _data = data;
  }

  BiNode(int data,  BiNode * left, BiNode * right) {
    _left = left;
    _right = right;
    _data = data;
  }

  int getData() { return _data; }

  BiNode * getLeft() {
    return _left;
  };

  BiNode * getRight() {
    return _right;
  };

  static BiNode* convertTreeToCircularList(BiNode* treeRoot) { 
    if (treeRoot == nullptr) return nullptr;
    
    BiNode * _left_head = convertTreeToCircularList(treeRoot->_left); //convert left sub tree to the cirular list
    BiNode * _right_head = convertTreeToCircularList(treeRoot->_right); //convert right sub tree to the cirular list


    if (_left_head != nullptr) {
      //connect left sub tree to the root
      BiNode * _left_tail = _left_head->_left;
      _left_tail->_right = treeRoot;
      treeRoot->_left = _left_tail;
    } else {
      _left_head = treeRoot; 
    }

    BiNode * _right_tail = nullptr;
    if (_right_head != nullptr) {
      //connect right sub tree to the root
      _right_tail = _right_head->_left;
      _right_head->_left = treeRoot;
      treeRoot->_right = _right_head;
    } else {
      _right_tail = treeRoot;
    }

    //connect head and tail
    _right_tail->_right = _left_head;
    _left_head->_left = _right_tail;

    return _left_head;    
  }

  static BiNode* convertTreeToList(BiNode* treeRoot) {
    BiNode* head = convertTreeToCircularList(treeRoot);
    if (head != nullptr) {
      //break the cycle 
      BiNode* tail = head->_left;
      tail->_right = nullptr;
      head->_left = nullptr;
    }
    return head;
  }
};