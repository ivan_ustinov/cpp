#include <array>
#include <bitset>
#include <cmath>
#include <ctime>
#include <iostream>
#include <forward_list>
#include <list>
#include <map>
#include <set>
#include <string>
#include <stack>
#include <unordered_set>
#include <unordered_map>

template <class T>
class VerticalListIterator {
  std::list<typename std::list<T>::iterator> listOfIterators;
  std::list<typename std::list<T>::iterator> listOfEnds;

  typename std::list<std::list<T>>::iterator itr;
public:
  VerticalListIterator(std::list<std::list<T>> *ln) {
    for (auto it = ln->begin(); it != ln->end(); ++it) {
      std::list<T>::iterator elemIt = it->begin();
      if (it->begin() != it->end()) {
        listOfEnds.push_back(it->end());
        listOfIterators.push_back(it->begin());
      }
    }
  };
  VerticalListIterator& operator++() {
    auto itT = listOfIterators.front();
    auto itEnd = listOfEnds.front();
    listOfIterators.pop_front();
    listOfEnds.pop_front();
    if (++itT != itEnd) {
      listOfIterators.push_back(itT);
      listOfEnds.push_back(itEnd);
    }
    return *this;
  }
  T& operator*() {
    auto itT = listOfIterators.front();
    return *itT;
  }
  bool empty() {
    return listOfIterators.empty();
  }

};


void reverse(char *str) {
  char * endPos = str;
  while (*endPos != 0) {
    ++endPos;
  }
  --endPos;
  while (str < endPos) {
    std::swap(*str, *endPos);
    ++str;
    --endPos;
  }
}

class Node {
public:
  Node * ptr1;
  Node * ptr2;
};

Node * recursiveCopy(Node * n, std::map<Node*, Node*> &map) {
  if (NULL == n) return NULL;
  std::map<Node*, Node*>::iterator it = map.find(n);
  if (it != map.end()) {
    Node * nn = new Node();
    //copy data from n to nn
    map[n] = nn;
    nn->ptr1 = recursiveCopy(n->ptr1, map);
    nn->ptr2 = recursiveCopy(n->ptr2, map);
  } else {
    return it->second;
  }
  return recursiveCopy(n, map);
};

Node * recursiveCopy(Node * n) {
  std::map<Node*, Node*> map;
  return recursiveCopy(n, map);
};

void swapInPlace(int &a, int &b) {
  a = a - b;
  b = a + b;
  a = b - a;

};

int smallestDelta(std::vector<int> a, std::vector<int> b) {
  int al = a.size();
  int bl = b.size();
  std::sort(a.begin(), a.end());
  std::sort(b.begin(), b.end());
  int aptr = 0;
  int bptr = 0;
  int min = std::numeric_limits<int>::max();
  while (aptr < al &&  bptr < bl) {
    if (min > std::abs(a[aptr] - b[bptr])) {
      min = std::abs(a[aptr] - b[bptr]);
    }
    if (a[aptr] < b[bptr]) {
      aptr++;
    } else {
      bptr++;
    }
  }
  return min;
}

std::vector<int> pickMiteratively(std::vector<int> original, int m) {
  std::srand((unsigned int)std::time(0));
  std::vector<int> subset;
  subset.resize(m);
  /* Fill in subset array with first part of original array*/
  for (int i = 0; i < m; i++) {
    subset[i] = original[i];
  }

  /*Go through rest of original array. */
  for (int i = m; i < (int)original.size(); ++i) {
    int k = (int)std::rand()*i / RAND_MAX; //Random# between 0 and i, inclusive
    if (k < m) {
      subset[k] = original[i];
    }
  }
  return subset;
}

