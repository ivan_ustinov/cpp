#pragma once
#include <algorithm>
#include <vector>

namespace IntersectionOfTwoSortedArrays {
  template<typename T>
  std::vector<T> stdIntersect(std::vector<T> first, std::vector<T> second) {
    std::vector<T> intersection(std::max(first.size(), second.size()));
    auto it = std::set_intersection(first.begin(), first.end(), second.begin(), second.end(), intersection.begin());
    intersection.resize(it - intersection.begin());
    return intersection;
  }

}