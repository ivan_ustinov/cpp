#pragma once
/**
* Definition for binary tree
* struct TreeNode {
*     int val;
*     TreeNode *left;
*     TreeNode *right;
*     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
* };
*/
class BSTIterator {
private:
  vector<TreeNode *> currentPath;

public:
  BSTIterator(TreeNode *root) {
    while (root != nullptr) {
      currentPath.push_back(root);
      root = root->left;
    }
  }

  /** @return whether we have a next smallest number */
  bool hasNext() {
    return !currentPath.empty();
  }

  /** @return the next smallest number */
  int next() {
    if (currentPath.size() < 1) return 0;

    int valReturn = currentPath.back()->val;
    TreeNode * next = nullptr;
    //take the leftmost in a right subtree
    if (currentPath.back()->right != nullptr) {
      next = currentPath.back()->right;
      while (next->left != nullptr) {
        currentPath.push_back(next);
        next = next->left;
      };
      currentPath.push_back(next);
      return valReturn;
    }

    //return to parent from left branch
    while (currentPath.size() > 1 && currentPath.back() != currentPath[currentPath.size() - 2]->left) {  //(std::next(currentPath.rbegin()))->left){
      currentPath.pop_back();
    };
    currentPath.pop_back();

    return valReturn;
  }
};

/**
* Your BSTIterator will be called like this:
* BSTIterator i = BSTIterator(root);
* while (i.hasNext()) cout << i.next();
*/