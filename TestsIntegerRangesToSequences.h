#pragma once
#include "IntegerRangesToSequences.h"
#include "gtest\gtest.h"

TEST(pbIntegerRangesToSequences, test1) {
  std::vector<int> v{1,2,3};
  std::string s("1-3");
  EXPECT_EQ(IntegerRangesToSequences::buildRanges(v), s);
}

TEST(pbIntegerRangesToSequences, test2) {
  std::vector<int> v{ 1,2,4 };
  std::string s("1-2,4");
  EXPECT_EQ(IntegerRangesToSequences::buildRanges(v), s);
}

TEST(pbIntegerRangesToSequences, test3) {
  std::vector<int> v{ 1, 10, 11, 12, 45, 46 };
  std::string s("1,10-12,45-46");
  EXPECT_EQ(IntegerRangesToSequences::buildRanges(v), s);
}

TEST(pbIntegerRangesToSequences, test4) {
  std::vector<int> v{ 1,2,3 };
  std::string s("1-3");
  EXPECT_EQ(IntegerRangesToSequences::buildRanges(v), s);
}

TEST(pbLenghtOfLongestSubstring, test5) {
  std::vector<int> v{ 1,2,3 };
  std::string s("1-3");
  EXPECT_EQ(IntegerRangesToSequences::buildRanges(v), s);
}

