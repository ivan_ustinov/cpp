#include "gtest/gtest.h"

#include <math.h>
#include <list>
#include <memory>
#include <unordered_set>
#include <unordered_map>

#ifndef TEST_FRIENDS
#define TEST_FRIENDS
#endif

#pragma once

using namespace std;
class BabyNames
{
  //17.7 Baby Names
  TEST_FRIENDS;
public:
  static unordered_map<string, int> cumulativeFrequency(const unordered_map<string, int> &base_frequency, const list<pair<string, string>>& synonyms) {
    unordered_map<string, shared_ptr<unordered_set<string>>> poolsOfSynonyms;
    for (unordered_map<string, int>::const_iterator i = base_frequency.cbegin(); i != base_frequency.cend(); ++i) {
      shared_ptr<unordered_set<string>> s(new unordered_set<string>{ i->first });
      poolsOfSynonyms[i->first] = move(s);
    }
    for (auto i = synonyms.begin(); i != synonyms.end(); ++i) {
      shared_ptr<unordered_set<string>> s1 = poolsOfSynonyms[i->first];
      shared_ptr<unordered_set<string>> s2 = poolsOfSynonyms[i->second];
      s1->insert(s2->begin(), s2->end());
      for(string s : *s1){
        poolsOfSynonyms[s] = s1;
      }
    }
    unordered_map<string, int> r;
    for (auto i = poolsOfSynonyms.begin(); i != poolsOfSynonyms.end(); ++i) {
      shared_ptr<unordered_set<string>> pool = i->second;
      const unordered_set<string>::iterator name = pool->begin();
      if (r.find(*name) == r.end()) {
        int f = 0;
        for (const string s : *pool) {
          f += base_frequency.at(s);
        }
        r.insert(pair<string,int>(*name, f));
      }
    }
    return r;
  }
};