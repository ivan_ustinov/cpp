#pragma once
#include <queue>

namespace BinaryTreeSerializer {
  class BinaryNode {
  public:
    BinaryNode * left;
    BinaryNode * right;
    int value;
    BinaryNode(int val) :value(val), left(nullptr), right(nullptr) {}
  };

  inline void appendToStream(BinaryNode * node, std::stringstream& ss, std::queue<BinaryNode *> &q) {
    ss << " ";
    if (node != nullptr) {
      q.push(node);
      ss << node->value;
    } else {
      ss << "n";
    }
  }

  std::string serialize(BinaryNode * root) {
    std::queue<BinaryNode *> q;
    std::stringstream ss;
    
    appendToStream(root, ss, q);
    while (!q.empty()) {
      BinaryNode * current = q.front();
      appendToStream(current->left, ss, q);
      appendToStream(current->right, ss, q);
      q.pop();
    }

    return ss.str();
  }

  BinaryNode * deserialize(const std::string &serializedStr) {
    std::istringstream iss(serializedStr);
    std::queue<BinaryNode *> q;
    BinaryNode * root = nullptr;

    std::string valueStr;

    iss >> valueStr;
    if (valueStr != "n") {
      root = new BinaryNode(stoi(valueStr));
      q.push(root);
    }

    while (!q.empty()) {
      BinaryNode * current = q.front();
      
      iss >> valueStr;
      if (valueStr != "n") {
        current->left = new BinaryNode(stoi(valueStr));
        q.push(current->left);
      }

      iss >> valueStr;
      if (valueStr != "n") {
        current->right = new BinaryNode(stoi(valueStr));
        q.push(current->right);
      }

      q.pop();
    }

    return root;
  }
}