#pragma once

#include "BabyNames.h"
#include "gtest\gtest.h"


TEST(pb17_7, test1) {
  unordered_map<string, int> base_frequency{ {"Ivan",3},{"Vanya",2},{"Alex",4},{"Alexandre",3}};
  list<pair<string, string>> synonyms{ {"Ivan","Vanya" },{"Alex","Alexandre"}};
  unordered_map<string, int> res = BabyNames::cumulativeFrequency(base_frequency, synonyms);

  EXPECT_EQ(res["Ivan"], 5);
  EXPECT_EQ(res["Alex"], 7);
}

TEST(pb17_7, test2) {
  unordered_map<string, int> base_frequency{ { "Ivan",3 },{ "Vanya",2 },{ "Alex",4 },{ "Alexandre",3 } };
  list<pair<string, string>> synonyms{ { "Ivan","Vanya" },{ "Alex","Alexandre" },{ "Alex","Ivan" } };
  unordered_map<string, int> res = BabyNames::cumulativeFrequency(base_frequency, synonyms);

  EXPECT_EQ(res["Alex"], 12);
}