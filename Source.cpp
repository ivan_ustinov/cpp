#include "gtest/gtest.h"

#include "TestsSegmentIntersection.h"

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);

  testing::GTEST_FLAG(filter) = "pb*";
  RUN_ALL_TESTS();

  std::getchar();

  return 0;
}
