#pragma once

#include "ReSpace.h"
#include "gtest\gtest.h"

TEST(pb17_13, test1) {
  std::string s = "abcdefghijklmn";
  std::unordered_set<std::string> dico;
  EXPECT_EQ(ReSpace::reSpace(s, dico), s.size());
}

TEST(pb17_13, test2) {
  std::string s = "abcdefghijklmn";
  std::string substr = "ijk";
  std::unordered_set<std::string> dico{ substr };
  EXPECT_EQ(ReSpace::reSpace(s, dico), 11);
}

TEST(pb17_13, test3) {
  std::string s = "ijkdefghijklmn";
  std::string substr = "ijk";
  std::unordered_set<std::string> dico{ substr };
  EXPECT_EQ(ReSpace::reSpace(s, dico), 8);
}

TEST(pb17_13, test4) {
  std::string s = "ijkdefghijklmn";
  std::string substr1 = "ijk";
  std::string substr2 = "def";
  std::unordered_set<std::string> dico{ substr1, substr2 };
  EXPECT_EQ(ReSpace::reSpace(s, dico), 5);
}

TEST(pb17_13, test5) {
  std::string s = "ijkdefghijklmn";
  std::string substr1 = "ijk";
  std::string substr2 = "defgh";
  std::string substr3 = "l";
  std::string substr4 = "mn";
  std::unordered_set<std::string> dico{ substr1, substr2, substr3, substr4 };
  EXPECT_EQ(ReSpace::reSpace(s, dico), 0);
}