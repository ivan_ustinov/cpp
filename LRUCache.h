#include <list>
#include <unordered_map>


class LRUCache {
public:
  std::unordered_map<int, std::list<std::pair<int, int>>::iterator> keysToPositionInQueue;
  std::list < std::pair<int,int> > cashedResultsList;
  int _capacity;
  LRUCache(int capacity) {
    _capacity = capacity;
  }

  int get(int key) {
    auto keyPos = keysToPositionInQueue.find(key);
    if (keyPos == keysToPositionInQueue.end()) {
      return -1;
    }
    int value = keyPos->second->second;
    put(key, value);
    return value;
  }

  void put(int key, int value) {
    auto keyPos = keysToPositionInQueue.find(key);
    if (keyPos != keysToPositionInQueue.end()) {
      //errase previous value
      auto listIt = keyPos->second;
      cashedResultsList.erase(listIt);
      keysToPositionInQueue.erase(keyPos->first);
    }

    if (cashedResultsList.size() >= (unsigned int)_capacity) {
      std::pair<int, int> pairToDelete = cashedResultsList.front();
      cashedResultsList.pop_front();
      keysToPositionInQueue.erase(pairToDelete.first);
    }

    cashedResultsList.push_back({key, value});
    keysToPositionInQueue.insert({ key, std::prev(cashedResultsList.end()) });
  }
};

/**
* Your LRUCache object will be instantiated and called as such:
* LRUCache obj = new LRUCache(capacity);
* int param_1 = obj.get(key);
* obj.put(key,value);
*/