#pragma once
#include <algorithm>
#include <vector>
#include <unordered_map>

namespace TopKFrequent{
  std::vector<int> topKFrequent(std::vector<int>& nums, int k) {
    std::unordered_map<int, int> numsToFrequency;
    for (int i : nums) {
      numsToFrequency[i]++;
    }
    std::vector<int> result;
    for (auto i : numsToFrequency) {
      result.push_back(i.first);
    }
    std::sort(result.begin(), result.end(), [&](int left, int right) {
      return numsToFrequency[left] > numsToFrequency[right];
    });
    result.resize(std::min(k, (int)numsToFrequency.size()));
    return result;
  }
}