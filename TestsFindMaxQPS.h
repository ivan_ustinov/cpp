#pragma once

#include "FindPeakQps.h"
#include "gtest\gtest.h"

TEST(pbQPS, test1) {
  FindPeakQps::Data q1;
  q1.start = 1;
  q1.finish = 3;
  q1.qps = 1;

  FindPeakQps::Data q2;
  q2.start = 2;
  q2.finish = 5;
  q2.qps = 2;

  FindPeakQps::Data q3;
  q3.start = 4;
  q3.finish = 6;
  q3.qps = 3;
  std::vector<FindPeakQps::Data> v{q1,q2,q3};
  EXPECT_EQ(FindPeakQps::FindPeakQps(v), 5);
}

TEST(pbQPS, test2) {
  FindPeakQps::Data q1;
  q1.start = 1;
  q1.finish = 3;
  q1.qps = 1;

  FindPeakQps::Data q2;
  q2.start = 4;
  q2.finish = 6;
  q2.qps = 2;

  std::vector<FindPeakQps::Data> v{ q1,q2};
  EXPECT_EQ(FindPeakQps::FindPeakQps(v), 2);
}