#pragma once

#include "KitMultiple.h"
#include "gtest\gtest.h"


TEST(pb17_9, test1) {
  EXPECT_EQ(KitMultiple::kitMultiple(1), 3);
}

TEST(pb17_9, test2) {
  EXPECT_EQ(KitMultiple::kitMultiple(2), 5);
}

TEST(pb17_9, test3) {
  EXPECT_EQ(KitMultiple::kitMultiple(3), 7);
}

TEST(pb17_9, test4) {
  EXPECT_EQ(KitMultiple::kitMultiple(4), 15);
}

TEST(pb17_9, test5) {
  EXPECT_EQ(KitMultiple::kitMultiple(5), 21);
}

TEST(pb17_9, test6) {
  EXPECT_EQ(KitMultiple::kitMultiple(6), 35);
}

TEST(pb17_9, test7) {
  EXPECT_EQ(KitMultiple::kitMultiple(7), 105);
}

TEST(pb17_9, test8) {
  EXPECT_EQ(KitMultiple::kitMultiple(8), 315);
}

TEST(pb17_9, test9) {
  EXPECT_EQ(KitMultiple::kitMultiple(9), 525);
}

TEST(pb17_9, test10) {
  EXPECT_EQ(KitMultiple::kthMultiple(12), 49);
}