#pragma once

#include "gtest/gtest.h"

#include <vector>

using namespace std;
namespace CircusTower{
    //17.8 Circus Tower
  struct Person {
    int weight;
    int height;
  };

  template< class RandomIt, class Compare >
  int lis(RandomIt first, RandomIt last, Compare comp) { //finds the LIS(Longest Increasing Subsequence) 
    int size = last - first;
    vector<int> lisValues(size); // a vector to store dynamic programming values  
    lisValues[0] = 1; //longest subsequense that terminates with first element has length 1
    for (RandomIt i = next(first); i != last; ++i) {
      int maxLength = 0;
      for (RandomIt j = first; j != i; ++j) {
        if (comp(*j, *i) && lisValues[j - first] > maxLength) maxLength = lisValues[j - first];
      }
      lisValues[i - first] = maxLength + 1;
    }
    return lisValues.back();
  }

  int circusTower(const vector<Person>& troops) {
    vector<Person> troopsCopy = troops;
    sort(troopsCopy.begin(), troopsCopy.end(), [](const Person& l, const Person& r) {
      return l.height < r.height;
    });
    return lis(troopsCopy.begin(), troopsCopy.end(), [](const Person& l, const Person& r) {
      return l.weight <= r.weight;
    });
  }
}