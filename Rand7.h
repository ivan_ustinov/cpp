#pragma once
#include <vector>
#include <stdlib.h>
#include <iostream>

class Rand7
{
private:
  Rand7();
  ~Rand7();
  //given function
  static int rand5() {
    return rand() % 5;
  }
  //implemented with a rand5
  static int rand7() {
    for (int r = 5 * rand5() + rand5();; r = 5 * rand5() + rand5()) {
      if (r < 21) return r % 7;
    }
  }
public:
  static int test() {
    std::vector<int> accum(7);
    for (int i = 0; i < 10000; ++i) {
      ++accum[rand7()];
    }
    for(auto i : accum) {
      std::cout << i << " ";
    }
    return 0;
  }
};

