#pragma once

#include <vector>
#include <list>
#include <unordered_set>

namespace CountTrianglesInGraph {

  int numberOfTriangles(const std::list<std::pair<int, int>> &edges) {
    std::vector<std::unordered_set<int>> mEdges; // sparse matrix

    for (std::pair<int, int> e : edges) {
      if (mEdges.size() <= (unsigned int)std::max(e.first, e.second)) mEdges.resize(std::max(e.first, e.second) + 1);
      mEdges[e.second].insert(e.first);
      mEdges[e.first].insert(e.second);
    }

    int numberOfTriangles = 0;

    for (std::pair<int, int> e : edges) {
      for (int neighbor : mEdges[e.first]) {
        auto it = mEdges[e.second].find(neighbor);
        if (e.first != e.second && e.first != neighbor && e.second != neighbor && mEdges[e.second].find(neighbor) != mEdges[e.second].end()) {
          ++numberOfTriangles;
        }
      }
    }
    return numberOfTriangles / 3;
  }

  int numberOfTrianglesWithMatrix(const std::list<std::pair<int, int>> &edges) {
    //Get the size of graph
    int N = 0;
    for (std::pair<int, int> e : edges) {
      int m = std::max(e.first, e.second);
      if (N < m) N = m;
    }
    ++N; //max index to size

    std::vector<std::vector<bool>> M(N);//matrix of edges
    for (unsigned int i = 0; i < M.size();++i) { M[i].resize(N); }

    //initialize the matrix
    for (std::pair<int, int> e : edges) {
      M[e.first][e.second] = true;
      M[e.second][e.first] = true;
    }

    int numberOfTriangles = 0;

    for (int unsigned i = 0; i < M.size(); ++i) {
      for (int unsigned j = i + 1; j < M.size(); ++j) {
        for (int unsigned k = j + 1; k < M.size(); ++k) {
          if (M[i][j] == true && M[j][k] == true && M[i][k] == true) numberOfTriangles++;
        }
      }
    }

    return numberOfTriangles;
  }
}