#pragma once

#include "FindTwoMax.h"
#include "gtest\gtest.h"

TEST(pbTwoMax, test1) {
  std::vector<int> v{34,432,672,123,1,2,3,5,76,8,8,34,1};
  int max1 = 0;
  int max2 = 0;
  FindTwoMax::FindTopTwo<int>(v, &max1, &max2);
  EXPECT_EQ(max1, 672);
  EXPECT_EQ(max2, 432);
}

TEST(pbTwoMax, test2) {
  std::vector<int> v{ 34,12,1,2,3,5,8,8,34,1 };
  int max1 = 0;
  int max2 = 0;
  FindTwoMax::FindTopTwo(v, &max1, &max2);
  EXPECT_EQ(max1, 34);
  EXPECT_EQ(max2, 34);
}

TEST(pbTwoMax, test3) {
  std::vector<int> v{ 1 };
  int max1 = 0;
  int max2 = 0;
  FindTwoMax::FindTopTwo(v, &max1, &max2);
  EXPECT_EQ(max1, 0);
  EXPECT_EQ(max2, 0);
}

TEST(pbTwoMax, test4) {
  std::vector<int> v;
  int max1 = 0;
  int max2 = 0;
  FindTwoMax::FindTopTwo(v, &max1, &max2);
  EXPECT_EQ(max1, 0);
  EXPECT_EQ(max2, 0);
}