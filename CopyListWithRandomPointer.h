/**
* 138. Copy List with Random Pointer
* A linked list is given such that each node contains an additional random pointer which could point to any node in the list or null.
* Return a deep copy of the list.
* Definition for singly-linked list with a random pointer. */
#include <unordered_map>

namespace CopyListWithRandomPointer {
  
  struct RandomListNode {
    int label;
    RandomListNode *next, *random;
    RandomListNode(int x) : label(x), next(NULL), random(NULL) {}
  };

  class Solution {
  public:
    static RandomListNode *copyRandomList(RandomListNode *head) {
      std::unordered_map<RandomListNode *, RandomListNode*> originalToCopy;

      RandomListNode * current = head;
      RandomListNode * newHead = nullptr;
      if (head != nullptr) {
        newHead = new RandomListNode(head->label);
        originalToCopy.insert({ head, newHead });
      }
      
      while (current != nullptr) {
        auto it = originalToCopy.find(current);
        RandomListNode * newNode = it->second;

        if(current->next != nullptr){
          auto itNext = originalToCopy.find(current->next);
          RandomListNode * newNextNode = nullptr;
          if (itNext == originalToCopy.end()) {
            newNextNode = new RandomListNode(current->next->label);
            originalToCopy.insert({ current->next,  newNextNode });
          } else {
            newNextNode = itNext->second;
          }
          newNode->next = newNextNode;
        }

        if (current->random != nullptr) {
          auto itRand = originalToCopy.find(current->random);
          RandomListNode * newRandNode = nullptr;
          if (itRand == originalToCopy.end()) {
            newRandNode = new RandomListNode(current->random->label);
            originalToCopy.insert({ current->random,  newRandNode });
          } else {
            newRandNode = itRand->second;
          }
          newNode->random = newRandNode;
        }

        current = current->next;
      }

      return newHead;
    }
  };
};