#include "AbPattern.h"

#include <assert.h>

AbPattern::AbPattern()
{
}

bool AbPattern::check(const std::string & pattern, const std::string & str){
  int a_n = 0, b_n = 0 ;
  for (unsigned int i = 0; i < pattern.size(); ++i) {
    switch (pattern[i])
    {
      case 'a': ++a_n;
        break;
      case 'b': ++b_n;
        break;
      default:
        assert(false); //pattern should contain only a and b
        break;
    }
  };

  //spetial case if a or b apear only 1 time, that means 'a' can contain all the string and 'b' will be empty.
  if (a_n == 1 || b_n == 1) return true;

  //spetial case if a or b doesn't appear
  if (a_n == 0) {
    assert(b_n == pattern.size());
    if (str.size() % b_n) return false; 
    int b_word_size = str.size() / b_n;
    return check(pattern, str, a_n, b_n, 0, b_word_size);
  }
  if (b_n == 0) {
    assert(a_n == pattern.size());
    if (str.size() % a_n) return false;
    int a_word_size = str.size() / a_n;
    return check(pattern, str, a_n, b_n, a_word_size, 0);
  }

  //iterate over all the possible length values of a_word_size and b_word_size
  for (unsigned int a_word_size = 2; a_word_size < str.size()/a_n; a_word_size++) {
    int b_total_length = str.size() - a_word_size*a_n;

  }

  return false;
}

bool AbPattern::check(const std::string & pattern, const std::string & str, int a_n, int b_n, int a_word_size, int b_word_size)
{
  return false;
}


AbPattern::~AbPattern()
{
}
