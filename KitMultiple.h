#pragma once
#include <list>
#include <limits>

using namespace std;

namespace KitMultiple {
  //17.9 Kit Multiple
  int kitMultiple(int k) {
    int l = (int)floor(k / 7);
    int a = l, b = l, c = l;
    int d = k % 7;
    if (d == 1 || d == 4 || d == 5)a++;
    if (d == 2 || d == 4 || d == 6)b++;
    if (d == 3 || d == 5 || d == 6)c++;
    return (int)(pow(3, a)*pow(5, b)*pow(7, c));
  }

  template<class It>
  void print(const It &begin, const It& end) {
    cout << "[";
    for (It i = begin; i != end; ++i) {
        cout << *i << " ";
    }
    cout << "]" << endl;
  }
  
  int kthMultiple(int k) {
    list<int> q3;
    list<int> q5;
    list<int> q7;
    q3.push_back(1);
    int val = 0;
    for (int i = 0; i < k; ++i) {
      int v3 = q3.empty() ? numeric_limits<int>::max() : q3.front();
      int v5 = q5.empty() ? numeric_limits<int>::max() : q5.front();
      int v7 = q7.empty() ? numeric_limits<int>::max() : q7.front();
      val = min(v3, min(v5, v7));
      print(q3.begin(), q3.end());
      print(q5.begin(), q5.end());
      print(q7.begin(), q7.end());
      cout << "val=" << val << endl;
      if (val == v3) {
        q3.pop_front();
        q3.push_back(3 * val);
        q5.push_back(5 * val);
      } else if (val == v5) {
        q5.pop_front();
        q5.push_back(5 * val);
      } else if (val == v7) {
        q7.pop_front();
      }
      q7.push_back(7 * val);
    }
    return val;
  }
}