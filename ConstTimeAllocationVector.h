#include "gtest/gtest.h"

#include <memory>

#define TEST_FRIENDS \
    friend class noninitialisation_ct_vector_Test;

#pragma once
template<typename T>
class ConstTimeAllocationVector
{
  TEST_FRIENDS;
private:
  struct WraperT
  {
    unsigned int dictionary_index;
    T t;
  };
  std::unique_ptr<unsigned int[]> dictionary; //indices of T in the vector
  std::unique_ptr<WraperT[]> vector;           //basic container for T values
  unsigned int initializedElementsCount = 0;
public:
  ConstTimeAllocationVector(int size): vector((WraperT*)::operator new(size*sizeof(WraperT))), dictionary((unsigned int*)::operator new(size*sizeof(WraperT))) {};
  T& get(unsigned int index) {
    if (!wasObjectInitialized(index)) {
      T t; 
      dictionary[initializedElementsCount] = index;
      WraperT wt;
      wt.dictionary_index = initializedElementsCount;
      wt.t = t;
      vector[index] = wt;
      ++initializedElementsCount;
    }
    return vector[index].t;
  }
private:
  inline bool wasObjectInitialized(unsigned int index) {
    return vector[index].dictionary_index < initializedElementsCount && dictionary[vector[index].dictionary_index] == index;
  };
};


class CustomInt {
public:
  int v;
  CustomInt() {
    v = 42;
    // std::cout << "CustomInt()" << std::endl;
  };
  CustomInt(int v) {
    this->v = v;
  };
};

TEST(ct_vector_test_case, ct_vector_test)
{
  ConstTimeAllocationVector<CustomInt> v(512);

  EXPECT_EQ(v.get(7).v, 42);

  v.get(7).v = 7;
  EXPECT_EQ(v.get(7).v, 7);
  v.get(6).v = 6;
  EXPECT_EQ(v.get(6).v, 6);
  v.get(5).v = 5;
  EXPECT_EQ(v.get(5).v, 5);
  EXPECT_EQ(v.get(4).v, 42);
}

TEST(noninitialisation, ct_vector)
{
  ConstTimeAllocationVector<CustomInt> v(512);
  EXPECT_NE(v.vector[0].t.v, 42);
  EXPECT_NE(v.vector[1].t.v, 42);
  EXPECT_NE(v.vector[2].t.v, 42);
  EXPECT_NE(v.vector[3].t.v, 42);
}
