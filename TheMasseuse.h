#pragma once
#include <vector>
namespace TheMasseuse {
  //17.16 The Masseuse
  int bestSchedule(const std::vector<int> &appointments) {
    if (appointments.size() < 1) return 0;
    int bestWithLast = appointments.back();
    if (appointments.size() < 2) return bestWithLast;
    int bestWitoutLast = 0;

    for (auto it = std::next(appointments.rbegin()); it != appointments.rend(); it++) {
      //new bestWithLast
      int newBestWithLast = bestWitoutLast + *it;
      bestWitoutLast = std::max(bestWithLast, bestWitoutLast);
      bestWithLast = newBestWithLast;
    }

    return std::max(bestWithLast, bestWitoutLast);
  }
}