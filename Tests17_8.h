#pragma once

#include "CircusTower.h"
#include "gtest\gtest.h"


TEST(pb17_8, test1) {
  CircusTower::Person a;
  a.height = 1;
  a.weight = 1;

  CircusTower::Person b;
  b.height = 2;
  b.weight = 2;

  CircusTower::Person c;
  c.height = 3;
  c.weight = 3;

  CircusTower::Person d;
  d.height = 4;
  d.weight = 4;

  vector<CircusTower::Person> troops{ a,b,c,d };

  EXPECT_EQ(CircusTower::circusTower(troops), 4);
}

TEST(pb17_8, test2) {

  CircusTower::Person a;
  a.height = 1;
  a.weight = 1;

  CircusTower::Person b;
  b.height = 1;
  b.weight = 1;

  CircusTower::Person c;
  c.height = 1;
  c.weight = 1;

  CircusTower::Person d;
  d.height = 1;
  d.weight = 1;

  vector<CircusTower::Person> troops{ a,b,c,d };

  EXPECT_EQ(CircusTower::circusTower(troops), 4);
}

TEST(pb17_8, test3) {
  CircusTower::Person a;
  a.height = 2;
  a.weight = 2;

  CircusTower::Person b;
  b.height = 1;
  b.weight = 1;

  CircusTower::Person c;
  c.height = 3;
  c.weight = 3;

  CircusTower::Person d;
  d.height = 4;
  d.weight = 4;

  vector<CircusTower::Person> troops{ a,b,c,d };

  EXPECT_EQ(CircusTower::circusTower(troops), 4);
}

TEST(pb17_8, test4) {
  CircusTower::Person a;
  a.height = 1;
  a.weight = 2;

  CircusTower::Person b;
  b.height = 2;
  b.weight = 1;

  CircusTower::Person c;
  c.height = 3;
  c.weight = 4;

  CircusTower::Person d;
  d.height = 4;
  d.weight = 3;

  vector<CircusTower::Person> troops{ a,b,c,d };

  EXPECT_EQ(CircusTower::circusTower(troops), 2);
}

TEST(pb17_8, test5) {
  CircusTower::Person a;
  a.height = 1;
  a.weight = 2;

  CircusTower::Person b;
  b.height = 2;
  b.weight = 1;

  CircusTower::Person c;
  c.height = 3;
  c.weight = 4;

  CircusTower::Person d;
  d.height = 4;
  d.weight = 5;

  vector<CircusTower::Person> troops{ a,b,c,d };

  EXPECT_EQ(CircusTower::circusTower(troops), 3);
}