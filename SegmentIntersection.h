#pragma once
#include <algorithm>
#include <vector>

namespace SegmentIntersection {
  std::pair<int, int> intersection(const std::pair<int, int> &left, const std::pair<int, int> &right) {
    return {std::min(left.first, right.first), std::max(left.second, right.second)};
  }

  bool hasIntersection(const std::pair<int, int> &left, const std::pair<int, int> &right) {
    return std::max(left.first, right.first) <= std::min(left.second, right.second);
  }

  std::vector<std::pair<int, int>> joinSegments(std::vector<std::pair<int, int>> segments) {
    std::sort(segments.begin(),segments.end(), [](const std::pair<int, int> & lhs, const std::pair<int, int> & rhs) {return lhs.first < rhs.first; });
    std::vector<std::pair<int, int>> result;
    std::pair<int, int> current = *segments.begin();
    for (auto it = std::next(segments.begin()); it != segments.end(); ++it) {
      if (hasIntersection(current, *it)) {
        current = intersection(current, *it);
      } else {
        result.push_back(current);
        current = *it;
      }
    }
    result.push_back(current);
    return result;
  }
}