#include "gtest/gtest.h"

#include <math.h>

#ifndef TEST_FRIENDS
#define TEST_FRIENDS
#endif

#pragma once
class CountTwos
{
  //17.6 Count of 2s
  TEST_FRIENDS;
private:
public:
  static int Count2s(int n) {
    int sum = 0;
    for (int i = 0; i < log10(n); ++i) {
      sum += (int)(leftPart(n, i)*pow(10, i));
      int iDigit = getDigit(n, i);
      if (iDigit > 2) {
        sum += (int)pow(10, i);
      }
      if (iDigit == 2) sum += (rightPart(n, i) + 1);
    }
    return sum;
  }
  static int Count2sBrutForce(int n) {
    int sum = 0;
    for (; n > 1; --n) {
      for (int i = 0; i < log10(n); ++i) {
        int iDigit = getDigit(n, i);
        if (iDigit == 2) sum++;
      }
    }
    return sum;
  }
private:
  static int rightPart(int n, int i) {
    if (i < 1) return 0;
    if (i > log10(n)) return n;
    return n % (int)pow(10, i);
  }
  static int leftPart(int n, int i) {
    if (i < 0) return n;
    if (i > log10(n)) return 0;
    return (int) floor( n / pow(10, i+1));
  }
  static int getDigit(int n, int i) {
    return (int)floor(n / pow(10, i)) % 10;
  }
};