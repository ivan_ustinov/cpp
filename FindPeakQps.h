#pragma once

#include <algorithm>
#include <vector>

namespace FindPeakQps {
  struct Data { 
    long start; 
    long finish; 
    long qps; 
  };

  long FindPeakQps(const std::vector<Data> &data) {
    std::vector<std::pair<long, long>> starts(data.size());
    std::vector<std::pair<long, long>> finishes(data.size());

    for (unsigned int i = 0; i < data.size();++i) {
      starts[i].first = data[i].start;
      starts[i].second = data[i].qps;
      finishes[i].first = data[i].finish;
      finishes[i].second = data[i].qps;
    }

    std::sort(starts.begin(), starts.end(), [](const std::pair<long, long> & left, const std::pair<long, long> & right) {return left.first < right.first; });
    std::sort(finishes.begin(), finishes.end(), [](const std::pair<long, long> & left, const std::pair<long, long> & right) {return left.first < right.first; });
    
    long maxQps = 0;
    long currentQps = 0;
    auto finish = finishes.begin();
    for (auto start = starts.begin(); start != starts.end(); ++start) {
      while (finish != finishes.end() && start->first >= finish->first) {
        currentQps -= finish->second;
        ++finish;
      }
      currentQps += start->second;
      if (currentQps > maxQps) maxQps = currentQps;
    }
    return maxQps;
  }
}