#pragma once

#include "SetNextForEachNode.h"
#include "gtest\gtest.h"

TEST(pbNextForEach, test1) {
  /*
  Example of a tree:

  a
  b   c    d
  e  f g  h i
  j       k
  */

  SetNextForEachNode::Node k(11);
  SetNextForEachNode::Node j(10);
  SetNextForEachNode::Node i(9);
  SetNextForEachNode::Node g(7);
  SetNextForEachNode::Node f(6);

  
  SetNextForEachNode::Node h(8);

  SetNextForEachNode::Node a(1);
  SetNextForEachNode::Node b(2);
  SetNextForEachNode::Node c(3);
  SetNextForEachNode::Node d(4);
  SetNextForEachNode::Node e(5);
  e.children.push_back(&j);
  b.children.push_back(&e);

  c.children.push_back(&f);
  c.children.push_back(&g);

  d.children.push_back(&h);
  d.children.push_back(&i);

  h.children.push_back(&k);

  a.children.push_back(&b);
  a.children.push_back(&c);
  a.children.push_back(&d);

  SetNextForEachNode::set_next_for_each_node(&a);
  int v = 0;
  for (SetNextForEachNode::Node * it = &a; it != nullptr; it = it->next) {
    EXPECT_EQ(it->_c, ++v);
  }
}