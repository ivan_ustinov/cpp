#pragma once
#include "CountTrianglesInGraph.h"
#include "gtest\gtest.h"

TEST(pbCountTrianglesInGraph, test1) {
  std::list<std::pair<int, int>> edges{{1,2},{1,3},{2,3},{0,0},{0,1}};
  EXPECT_EQ(CountTrianglesInGraph::numberOfTriangles(edges), 1);
}

TEST(pbCountTrianglesInGraph, test2) {
  std::list<std::pair<int, int>> edges{ { 1,2 },{ 1,3 },{ 2,3 },{ 0,0 },{ 0,1 } };
  EXPECT_EQ(CountTrianglesInGraph::numberOfTrianglesWithMatrix(edges), 1);
}