#pragma once
/*
Set the `next` pointers for the each node within the given tree:
- The `next` pointer of the given node should point to the next node (on the right) within the current level;
- If there is no next node on the current level, `next` should point to the first node on the next level;
- If there is no next level, `next` should be NULL.
*/

/*
Example of a tree:

a
b   c    d
e  f g  h i
j       k
*/
#include <vector>

namespace SetNextForEachNode {
  struct Node {
    int _c;
    Node* next;
    std::vector<Node*> children;
    Node(int c) { _c = c; }
  };

  void set_next_for_each_node(Node* root) {
    if (root->children.size() == 0) {
      root->next = nullptr;
      return;
    }

    Node *prev = root;
    Node *currentNode = root;
    do{
      for (unsigned int i = 0; i < currentNode->children.size(); ++i) {
        prev->next = currentNode->children[i];
        prev = prev->next;
      }
      currentNode = currentNode->next;
    } while (currentNode != prev);
    
    prev->next = nullptr;
  }
}