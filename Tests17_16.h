#pragma once

#include "TheMasseuse.h"
#include "gtest\gtest.h"

TEST(pb17_16, test1) {
  std::vector<int> v{ 30,15,60,75,45,15,15,45 };
  EXPECT_EQ(TheMasseuse::bestSchedule(v), 180);
}

TEST(pb17_16, test2) {
  std::vector<int> v{ 30,15,60 };
  EXPECT_EQ(TheMasseuse::bestSchedule(v), 90);
}

TEST(pb17_16, test3) {
  std::vector<int> v{ 30,15 };
  EXPECT_EQ(TheMasseuse::bestSchedule(v), 30);
}