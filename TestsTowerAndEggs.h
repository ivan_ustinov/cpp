#pragma once

#include "TowerAndEggs.h"
#include "gtest\gtest.h"


TEST(pbTower, test1) {
  EXPECT_EQ(TowerAndEggs::getNumberOfThrows(1), 1);
}

TEST(pbTower, test2) {
  EXPECT_EQ(TowerAndEggs::getNumberOfThrows(2), 2);
}

TEST(pbTower, test3) {
  EXPECT_EQ(TowerAndEggs::getNumberOfThrows(3), 2);
}

TEST(pbTower, test4) {
  EXPECT_EQ(TowerAndEggs::getNumberOfThrows(4), 3);
}

TEST(pbTower, test5) {
  EXPECT_EQ(TowerAndEggs::getNumberOfThrows(100), 14);
}