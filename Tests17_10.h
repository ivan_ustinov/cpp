#pragma once

#include "MajorityElement.h"
#include "gtest\gtest.h"


TEST(pb17_10, test1) {
  vector<int> v{ 4,4,3,4 };
  EXPECT_EQ(MajorityElement::majorityElement(v), 4);
}

TEST(pb17_10, test2) {
  vector<int> v{ 1,1,4,4,3,4 };
  EXPECT_EQ(MajorityElement::majorityElement(v), -1);
}

TEST(pb17_10, test3) {
  vector<int> v{ 1,1,4,4,3,4,5,5,5,5,5,5,5};
  EXPECT_EQ(MajorityElement::majorityElement(v), 5);
}