#pragma once

#include "BinaryTreeSerializer.h"
#include "gtest\gtest.h"

TEST(pbBinaryTreeSerializer, test1) {
  BinaryTreeSerializer::BinaryNode n1(1);
  BinaryTreeSerializer::BinaryNode n2(2);
  BinaryTreeSerializer::BinaryNode n3(3);
  BinaryTreeSerializer::BinaryNode n4(4);

  n1.right = &n2;
  n2.left = &n3;
  n2.right = &n4;

  std::string s = BinaryTreeSerializer::serialize(&n1);

  EXPECT_EQ(s, " 1 n 2 3 4 n n n n");

}


TEST(pbBinaryTreeSerializer, test2) {
  std::string s(" 1 n 2 3 4 n n n n");
  BinaryTreeSerializer::BinaryNode * deserializedRoot = BinaryTreeSerializer::deserialize(s);
  EXPECT_EQ(deserializedRoot->value, 1);
  EXPECT_EQ(deserializedRoot->left, nullptr);
  EXPECT_EQ(deserializedRoot->right->value, 2);
  EXPECT_EQ(deserializedRoot->right->left->value, 3);
  EXPECT_EQ(deserializedRoot->right->right->value, 4);
}