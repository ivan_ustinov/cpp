#pragma once
#include <algorithm>
#include <string>
#include <unordered_set>

namespace ReSpace {
  //17.13 ReSpace
  int reSpace(const std::string &text, const std::unordered_set<std::string> &dico) {
    std::vector<int> minScore(text.size()+1); //dynamic programming buffer. At i there is a subsolution for subtext from i to the end
    std::vector<int> mapWordScore(dico.size()+1);//Keeps a temporary score for a word
    minScore[text.size()] = 0;
    for (int i = text.size() - 1; i >= 0; --i) {
      int j = 1;
      mapWordScore[0] = minScore[i + 1] + 1;
      for (std::string word : dico) {
        if (text.compare(i, word.size(), word) == 0) { // check if from position i we can use the word. 
          mapWordScore[j] = minScore[i + word.size()]; // score is equal to the best score that we can get for the rest of the text
        } else {
          mapWordScore[j] = std::numeric_limits<int>::max();
        }
        ++j;
      }
      minScore[i] = *(std::min_element(mapWordScore.begin(), mapWordScore.end()));
    }
    return minScore[0];
  }
}