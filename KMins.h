#pragma once

#include <vector>

namespace KMins {
  bool myIntCompare(int left, int right) { return left < right; }

  void kminsInPlace(std::vector<int>& data, int k) {
    //O(n*log(K)) runtime solution 
    if (k < 1 || k >= data.size()) return;
    std::make_heap(data.begin(), data.begin() + k, &myIntCompare);
    for (int i = k; i < data.size(); ++i) {
      //get the max from heap 
      if (data.front() > data[i]) {
        std::pop_heap(data.begin(), data.begin() + k, &myIntCompare);
        int tmp = data[i];
        data[i] = data[k - 1];
        data[k - 1] = tmp;
        std::push_heap(data.begin(), data.begin() + k, &myIntCompare);
      }
    } 
  }
}