#pragma once

namespace AddTwoListedNumbers {
  //Definition for singly-linked list.
  struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
  };

  class Solution {
  public:
    static ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
      /*int v1 = readNumber(l1);
      int v2 = readNumber(l2);
      int sum = v1+v2;
      return storeNumber(sum);*/
      int d1 = l1->val;
      int d2 = l2->val;
      int d = (d1 + d2) % 10;
      int carrier = d1 + d2 > 9 ? 1 : 0;
      ListNode * head = new ListNode(d);
      ListNode * prev = head;
      l1 = l1->next;
      l2 = l2->next;
      for (; l1 != NULL || l2 != NULL;) {
        if (l1 != NULL) {
          d1 = l1->val;
          l1 = l1->next;
        } else {
          d1 = 0;
        }
        if (l2 != NULL) {
          d2 = l2->val;
          l2 = l2->next;
        } else {
          d2 = 0;
        }

        int d = (d1 + d2 + carrier) % 10;
        carrier = d1 + d2 + carrier > 9 ? 1 : 0;
        ListNode * iNode = new ListNode(d);
        prev->next = iNode;
        prev = iNode;
      }
      if (carrier != 0) { prev->next = new ListNode(1); }
      return head;
    }

    static int readNumber(ListNode *l) {
      int val = 0;
      int pow10 = 1;
      for (ListNode *current = l; current != nullptr; current = current->next) {
        val += current->val*pow10;
        pow10 *= 10;
      }
      return val;
    }

    static ListNode *storeNumber(int val) {
      if (val < 10) return new ListNode(val); //zero case
      const int size = (int)floor(log10(val)) + 1;/*
      char * buffer = new char[size*sizeof(ListNode)];
      int d = val % 10;
      for (char* i = buffer; i < buffer + size*sizeof(ListNode); i += sizeof(ListNode)) {
        ListNode * iNode = new(i) ListNode(d);
        iNode->next = (ListNode *)(i + sizeof(ListNode));
        val /= 10;
        d = val % 10;
      }
      ListNode * last = (ListNode *)(buffer + (size - 1)*sizeof(ListNode));
      last->next = nullptr;
      return (ListNode *)buffer;*/     
      int d = val % 10;
      val /= 10;
      ListNode * head = new ListNode(d);
    
      for (ListNode * prev = head; val > 0;) {
        d = val % 10;
        val /= 10;
        ListNode * iNode = new ListNode(d);
        prev->next = iNode;
        prev = iNode;
      }
      return head;
    }
  };
}