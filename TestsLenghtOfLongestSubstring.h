#pragma once
#include "LenghtOfLongestSubstring.h"
#include "gtest\gtest.h"

TEST(pbLenghtOfLongestSubstring, test1) {
  std::string s("ab");
  EXPECT_EQ(LenghtOfLongestSubstring::lenghtOfLongestSubstring(s), 2);
}

TEST(pbLenghtOfLongestSubstring, test2) {
  std::string s("aaabbaacccadkfk");
  EXPECT_EQ(LenghtOfLongestSubstring::lenghtOfLongestSubstring(s), 7);
}

TEST(pbLenghtOfLongestSubstring, test3) {
  std::string s("aaaa");
  EXPECT_EQ(LenghtOfLongestSubstring::lenghtOfLongestSubstring(s), 4);
}

TEST(pbLenghtOfLongestSubstring, test4) {
  std::string s("abababa");
  EXPECT_EQ(LenghtOfLongestSubstring::lenghtOfLongestSubstring(s), 7);
}

TEST(pbLenghtOfLongestSubstring, test5) {
  std::string s("aaabbaacccadkfkkfkkfkkfkkfk");
  EXPECT_EQ(LenghtOfLongestSubstring::lenghtOfLongestSubstring(s), 15);
}

