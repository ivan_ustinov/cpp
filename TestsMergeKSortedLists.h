#pragma once

#include "MergeKSortedLists.h"
#include "gtest\gtest.h"

TEST(pbMergeKSortedLists, test1) {
  //[[0,2,4]]
  MergeKSortedLists::ListNode * head1 = new MergeKSortedLists::ListNode(0);
  head1->next = new MergeKSortedLists::ListNode(2);
  head1->next->next = new MergeKSortedLists::ListNode(4);
  std::vector<MergeKSortedLists::ListNode *> lists;
  lists.push_back(head1);
  MergeKSortedLists::ListNode * head = MergeKSortedLists::Solution::mergeKLists(lists);
  int v = -2;
  for (MergeKSortedLists::ListNode * it = head; it != nullptr; it = it->next) {
    EXPECT_EQ(it->val, v+=2);
  }
}

TEST(pbMergeKSortedLists, test2) {
  //[[0][1]]
  MergeKSortedLists::ListNode * head1 = new MergeKSortedLists::ListNode(0);
  MergeKSortedLists::ListNode * head2 = new MergeKSortedLists::ListNode(1);
  std::vector<MergeKSortedLists::ListNode *> lists{head1,head2};

  MergeKSortedLists::ListNode * head = MergeKSortedLists::Solution::mergeKLists(lists);
  int v = 0;
  for (MergeKSortedLists::ListNode * it = head; it != nullptr; it = it->next) {
    EXPECT_EQ(it->val,v++);
  }
}