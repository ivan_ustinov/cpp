#pragma once

#include "gtest/gtest.h"

#include <math.h>

using namespace std;
namespace TowerAndEggs
{
  int getNumberOfThrows(int n) {
    vector<int> nOfThrows(n+1); // buffer that holds the dynamic programming structure(including no floors at 0)
    nOfThrows[0] = 0; 
    nOfThrows[1] = 1;
    for (int n_floors = 2; n_floors <= n; ++n_floors) { //resolve the problem for the all possible number of floors
      int minThrows = n_floors; //upper bound for the number of throws is the number of floors
      for (int x = 0; x < n_floors; ++x) { //test every floor
        int xn = max(x + 1, 1 + nOfThrows[n_floors - x - 1]);
        if( xn < minThrows) minThrows = xn;
      }
      nOfThrows[n_floors] = minThrows;
    }
    return nOfThrows[n];
  }
};